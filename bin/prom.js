const Sequelize = require('sequelize')
const Promise = Sequelize.Promise

const config = require('../src/lib/config')
const cl = console.log
const qryOpt = {
  raw: true,
  logging: false
}

const squel = new Sequelize(config.mssql)
const runQry = (qry) => {
  return squel.query(qry, qryOpt)
}

const arrVal = [
  `
  INSERT INTO import_ticket_tbl(dim_project_id, ticket_active, ticket_number, ticket_created_at, ticket_resolved_at, ticket_closed_at, ticket_knowledge, dim_ticket_state_id, dim_ticket_type_id, dim_ticket_contact_id, dim_ticket_priority_id, dim_ticket_urgency_id, dim_ticket_impact_id, dim_ticket_category_id, dim_ticket_subcategory_id, dim_business_service_id, dim_configuration_item_id, reasignment_count, dim_resolution_code_id, dim_resolution_method_id, opened_by_id, assignment_group_id, assigned_to_id, resolved_by_id, closed_by_id)
  VALUES
  ('undefined', 'FALSE', 'TS2758195', '2018-01-01 12:25:00', '2018-01-02 14:12:00', '2018-01-08 12:35:00', 'FALSE', 'Closed', 'Technical Support Ticket', 'Phone', 'Non-Priority', '3 - Low', 'Low', 'Classroom', 'OV', 'unknown', 'unknown', 0, 'Issue: Known/Escalated', 'undefined', 'unknown', 'Technical Support', 'Jhonathan Navarro', 'Charlie Jay Sibug', 'Charlie Jay Sibug'),
  ('undefined', 'FALSE', 'TS2758211', '2018-01-01 13:47:00', '2018-01-01 14:07:00', '2018-01-07 12:35:00', 'FALSE', 'Closed', 'Technical Support Ticket', 'E-mail', 'Non-Priority', '3 - Low', 'Low', 'Additional Inquiry Types', 'Ghost/Spam/Wrong #', 'unknown', 'unknown', 0, 'Duplicate/Spam Ticket', 'unknown', 'unknown', 'Technical Support', 'Mico Valencia', 'Mico Valencia', 'Mico Valencia')
  `,
  `
  INSERT INTO import_ticket_tbl(dim_project_id, ticket_active, ticket_number, ticket_created_at, ticket_resolved_at, ticket_closed_at, ticket_knowledge, dim_ticket_state_id, dim_ticket_type_id, dim_ticket_contact_id, dim_ticket_priority_id, dim_ticket_urgency_id, dim_ticket_impact_id, dim_ticket_category_id, dim_ticket_subcategory_id, dim_business_service_id, dim_configuration_item_id, reasignment_count, dim_resolution_code_id, dim_resolution_method_id, opened_by_id, assignment_group_id, assigned_to_id, resolved_by_id, closed_by_id)
  VALUES
  ('undefined', 'FALSE', 'TS2758215', '2018-01-01 15:02:00', '2018-01-01 15:21:00', '2018-01-07 12:35:00', 'FALSE', 'Closed', 'Technical Support Ticket', 'Phone', 'Non-Priority', '3 - Low', 'Low', 'Portal', 'Academics', 'unknown', 'unknown', 0, 'Action Needed by Other Dept', 'unknown', 'unknown', 'Technical Support', 'Jennifer Canonigo', 'Jennifer Canonigo', 'Jennifer Canonigo'),
  ('undefined', 'FALSE', 'TS2758216', '2018-01-01 15:19:00', '2018-01-01 15:21:00', '2018-01-13 12:35:00', 'FALSE', 'Closed', 'Technical Support Ticket', 'E-mail', 'Non-Priority', '3 - Low', 'Low', 'Classroom', 'Intellipath', 'unknown', 'unknown', 0, 'unknown', 'unknown', 'unknown', 'Technical Support', 'Mico Valencia', 'unknown', 'unknown')
  `
]

function spAfterBatch () {
  const spArrBatch = ['uspInsertImportToTmpTicket', 'uspInsertTmpToFactTicket', 'uspTruncateTmpFactTicket', 'uspResetTmpFactTcktIdentity', 'uspTruncateImportTicketTbl']
  return Promise.reduce(spArrBatch, (ind, sp) => {
    return squel.query(`EXEC ${sp}`, {logging: false}).then(() => {
      cl(`done running ${sp}`)
      return ind + 1
    })
  }, 0)
}

const runApp = async () => {
  try {
    await Promise.reduce(arrVal, (indx, val) => {
      return runQry(val).then((result) => {
        cl('then after qryInsert', result)
        return spAfterBatch().then(ind => {
          cl(`done spAfterBatch: ${ind}`)
          return indx + 1
        })
      })
    }, 0).then(indx => {
      cl('then of the reduce', indx)
    })
    cl('after reduce')
    process.exit(0)
  } catch (error) {
    if (error) {
      squel.close()
    }
  }
}

runApp()
