#!/usr/bin/env node

// const program = require('commander')
const chalk = require('chalk')
const figlet = require('figlet')

const { clearConsole } = require('../src/packages/shared-utils')
require('../src/lib/colors')
const app = require('../src/main')
const cl = console.log

clearConsole()
cl(
  chalk.blue(
    figlet.textSync('TRY-CLI', {horizontalLayout: 'full'})
  )
)

app()
