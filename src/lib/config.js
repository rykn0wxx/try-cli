module.exports = {
  mssql: {
    host: 'localhost',
    database: 'SandBoxDB',
    username: 'sa',
    password: '666123',
    port: 62412,
    dialect: 'mssql',
    dialectOptions: {
      requestTimeout: 90000,
      timeout: 90000,
      connectTimeout: 30000,
      cancelTimeout: 30000,
    },
    logging: false,
    pool: {
      max: 10,
      idle: 60000,
      acquire: 90000
    }
  },
  xlsx: {
    type: 'binary',
    cellNF: true,
    cellDates: true
  },
  chunkSize: 500
}
