const inquirer = require('inquirer')
const prompt = inquirer.prompt

const { fileExist } = require('../packages/shared-utils')

function _runValidation (val) {
  return (val.length) ? true : 'Did you enter/select anything...'
}

module.exports = {
  async askWhatTodo () {
    const { todo } = await prompt([{
      name: 'todo',
      type: 'list',
      message: 'What task would you like to do? ',
      choices: ['Test', 'Import Data', 'Exit']
    }])
    return todo
  },
  async askFileInfo () {
    const tmpFile = `C:\\Sites\\try-cli\\src\\raw_files\\ticket_worker.xlsx`
    const fileInfo = await prompt([{
      name: 'fileContent',
      type: 'list',
      message: 'Select what type of data will you import: ',
      choices: [
        new inquirer.Separator(' => File Content <= '),
        {name: 'phone'},
        {name: 'ticket'}
      ],
      validate: _runValidation
    }, {
      name: 'fileLoc',
      type: 'input',
      message: 'Enter full path of the file you wish to import',
      default: tmpFile,
      validate: (val) => {
        return (fileExist(val)) ? true : 'File does not exist'
      }
    }])
    return fileInfo
  }
}
