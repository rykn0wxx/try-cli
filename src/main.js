const { exit } = require('./packages/shared-utils')
const questions = require('./lib/questions')
const config = require('./lib/config')
const cl = console.log

function xlToSql (fileInfo) {
  const exlToSql = require('./packages/exl-to-sql/exl-to-sql')
  return new exlToSql(fileInfo, config.xlsx)
}

function dataFlow (fileContent) {
  const dtaFlow = require('./packages/data-flow')
  return new dtaFlow(config.mssql, fileContent)
}

async function runTestConnection () {
  const testConnection = require('./packages/test-connection/test-connection')
  await testConnection(config.mssql)
}

async function runImportData () {
  const fileInfo = await questions.askFileInfo()
  const ExlToSql = xlToSql(fileInfo)
  const insertQry = ExlToSql.genInsertSql({ schunkSize: config.chunkSize })
  const DataFlow = dataFlow(fileInfo.fileContent)
  await DataFlow.runPreImport()
  await DataFlow.runBatchImport(insertQry.sqlInsert, insertQry.sqlValues)
  await DataFlow.disconnect()
}

const runApp = async () => {
  try {
    const ansTodo = await questions.askWhatTodo()
    switch (ansTodo) {
      case 'Test':
        await runTestConnection()
        exit(0)
        break
      case 'Import Data':
        await runImportData()
        cl(`Will import some data`.info)
        exit(0)
        break
      default:
        cl(`Goodbye...`.info)
        exit(0)
        break
    }
  } catch (err) {
    if (err) {
      cl(`An error occured. `.error, err)
    }
  }
}

module.exports = runApp
