const XLSX = require('xlsx')
const _ = require('lodash')

class ExlUtil {
  readFile (xlFile, config) {
    return XLSX.readFile(`${xlFile}`, config)
  }
  asCsv (wb) {
    const result = _.stubArray()
    result.push(XLSX.utils.sheet_to_csv(wb.Sheets[wb.SheetNames[0]]))
    return _.join(result, `\n`)
  }
}

module.exports = class ExlToSql extends ExlUtil {
  constructor (fileInfo, config) {
    super()
    this.importType = fileInfo.fileContent
    this.location = fileInfo.fileLoc
    this.config = config
    this.wb = this.readFile(this.location)
    this.csv = this.asCsv(this.wb)
  }

  genInsertSql ({tblName, chunkSize}) {
    const arr_sql_val = _.stubArray()
    const csv_data_insert = this.csv.replace(/'|’/g, `''`)
    const csv_arr = csv_data_insert.split(/\r\n|\n\r|\n|\r/g)
    const cols = _.split(csv_arr[0], ',')
    const cols_length = cols.length
    let col_type_arr = _.stubArray()

    for (let i = 0; i < cols_length; i++) {
      cols[i] = cols[i].replace(/[^a-z0-9_$ ]/gi, '')
      cols[i] = cols[i].replace(/ /g, '_')
      cols[i] = ((_.trim(cols[i]) === '') ? (`column_${i}`) : _.trim(cols[i]))
    }

    const csvArr = _.filter(csv_arr, (e) => e.replace(/(\r\n|\n|\r)/gm, ''))
    if (csvArr[1] !== undefined) {
      col_type_arr = _.split(csvArr[1], ',')
    } else {
      col_type_arr = _.split(csvArr[0], ',')
    }

    let is_col_number = _.stubArray()
    for (let i = 0; i < cols_length; i++) {
      is_col_number[i] = _.isNumber(col_type_arr[i])
    }

    const col_names = _.join(cols, ', ')
    for (let i = 1; i < csvArr.length; i++) {
      const curElem = csvArr[i]
      const rowValueArr = _.stubArray()
      const valueArr = _.split(curElem, /(?!\B"[^"]*),(?![^"]*"\B)/g)
      let row = _.stubString()

      for (let j = 0; j < cols_length; j++) {
        if (is_col_number[j] === _.isNumber(valueArr[j])) {
          if (is_col_number[j]) {
            rowValueArr[j] = valueArr[j]
          } else {
            const tmpIsNumber = (valueArr[j] !== undefined) ? valueArr[j] : ''
            rowValueArr[j] = `'${tmpIsNumber}'`
          }
        } else {
          if (is_col_number[j]) {
            rowValueArr[j] = 0
          } else {
            const tmpNotIsNumber = (valueArr[j] !== undefined) ? valueArr[j] : ''
            rowValueArr[j] = `'${tmpNotIsNumber}'`
          }
        }
      }
      row = _.join(rowValueArr, ', ')
      arr_sql_val.push(`(${row})`)
    }
    // const tbl_name = `import_${tblName ? tblName : this.importType}_tbl`
    const tbl_name = tblName ? tblName : `import_${this.importType}_tbl`
    const insert = `INSERT INTO ${tbl_name} (${col_names})\nVALUES\n`
    return {
      sqlInsert: insert,
      sqlValues: _.chunk(arr_sql_val, chunkSize || 500)
    }
  }
}
