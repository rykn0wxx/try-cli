const Sequelize = require('sequelize')

const cl = console.log

module.exports = async (config) => {
  const sequelize = new Sequelize(config)
  await sequelize.authenticate()
    .then(() => {
      cl('Connected successfully'.good)
    }).finally(() => {
      sequelize.close()
    })
}
