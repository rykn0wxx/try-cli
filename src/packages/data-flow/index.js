const Sequelize = require('sequelize')
const Promise = Sequelize.Promise
const _ = require('lodash')

const { ctrlFlow } = require('../shared-utils')
const cl = console.log

const runStoredProc = (sequelize, spName) => {
  // cl('before await runStoredProc'.debug)
  return sequelize.query(`EXEC ${spName}`, { logging: false })
}

const runRawQuery = (sequelize, qry) => {
  return sequelize.query(qry, { raw: true, logging: false })
}

function uspBatchRun (sequelize, type) {
  const uspBatchArr = ctrlFlow('uspTmpToFact', type)
  return Promise.reduce(uspBatchArr, (ind, usp) => {
    return sequelize.query(`EXEC ${usp}`, {logging: false}).then(() => {
      // cl(`done running ${usp}`.debug)
      return ind + 1
    })
  }, 0)
}

module.exports = class DataFlow {
  constructor (config, type) {
    this.config = config
    this.sequelize = new Sequelize(config)
    this.type = type
  }

  async disconnect () {
    await this.sequelize.close()
  }

  async runPreImport () {
    let success = 0
    cl('Started running pre import processes'.info)
    const preImportArr = ctrlFlow('uspArr', this.type)
    await Promise.reduce(preImportArr, (ind, usp) => {
      return runStoredProc(this.sequelize, usp).then(() => {
        // cl(`Done exec stored proc: ${usp}`.debug)
        return ind + 1
      })
    }, 0).then((ind) => {
      success = ind
    }).finally(() => {
      cl(`Ended running ${success} pre import processes`.good)
    })
  }

  async runBatchImport (sqlInsert = '', chunks = []) {
    const sequelize = this.sequelize
    cl('Started running batch imports'.info, Date())
    const insertChunks = _.map(chunks, (chunk) => `${sqlInsert} ${chunk}`)
    await Promise.reduce(insertChunks, (indx, chunk) => {
      return runRawQuery(sequelize, chunk).then((chunkResult) => {
        cl(`then after qryInsert: ${chunkResult[1]}`.debug)
        return uspBatchRun(sequelize, this.type).then(ind => {
          cl(`done uspBatchRun: ${ind}`.debug)
          return indx + 1
        })
      })
    }, 0).then(indx => {
      cl(`Ended running ${indx} batch imports`.info, Date())
    })
  }

}
