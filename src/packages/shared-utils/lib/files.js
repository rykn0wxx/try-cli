const fs = require('fs')
const path = require('path')

exports.getCurDirBase = () => {
  return path.basename(process.cwd())
}

exports.dirExist = (dirPath) => {
  try {
    return fs.statSync(dirPath).isDirectory()
  } catch (e) {
    return false
  }
}

exports.fileExist = (filePath) => {
  try {
    return fs.statSync(filePath).isFile()
  } catch (e) {
    return false
  }
}
