const _ = require('lodash')

const flows = {
  uspArr: {
    phone: ['uspDeleteTmpFactCall', 'uspResetTmpFactIdentity', 'uspTruncateImportPhoneTbl'],
    ticket: ['uspTruncateTmpFactTicket', 'uspResetTmpFactTcktIdentity', 'uspTruncateImportTicketTbl']
  },
  uspImportToTmp: {
    phone: 'uspInsertImportToTmp',
    ticket: 'uspInsertImportToTmpTicket'
  },
  uspTmpToFact: {
    phone: '',
    ticket: ['uspInsertImportToTmpTicket', 'uspInsertTmpToFactTicket', 'uspTruncateTmpFactTicket', 'uspResetTmpFactTcktIdentity', 'uspTruncateImportTicketTbl']
  }
}

exports.ctrlFlow = (proc, type) => {
  return _.get(flows, `${proc}.${type}`)
}
