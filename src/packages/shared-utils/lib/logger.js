const chalk = require('chalk')
const readline = require('readline')
const padStart = require('string.prototype.padstart')
const EventEmitter = require('events')

exports.events = new EventEmitter()
const cl = console.log

const chalkTag = msg => chalk.bgBlackBright.white.dim(` ${msg} `)

function _log (type, tag, message) {
  if (message) {
    exports.events.emit('log', {
      message,
      type,
      tag
    })
  }
}

const format = (label = '', msg = '') => {
  return msg.split('\n').map((line, i) => {
    return i === 0 ? `${label} ${line}` : padStart(line, chalk.reset(label).length)
  }).join('\n')
}

exports.log = (msg = '', tag = null) => {
  tag ? cl(format(chalkTag(tag), msg)) : cl(msg)
  _log('log', tag, msg)
}

exports.clearConsole = title => {
  if (process.stdout.isTTY) {
    const blank = '\n'.repeat(process.stdout.rows)
    cl(blank)
    readline.cursorTo(process.stdout, 0, 0)
    readline.clearScreenDown(process.stdout)
    if (title) {
      cl(title)
    }
  }
}
